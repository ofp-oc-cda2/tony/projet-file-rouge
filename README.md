# Help your Meal

## Qu'est ce que c'est ?

Trop souvent, des aliments que nous avons acheté finissent perdu au fond d'un placard. Trop souvent nous nous rendons compte quand les magasins sont fermé que nous n'avons plus de lait. Et combien de fois restons nous devant les différents ingrédient présent dans notre réfrigérateur sans savoir quoi en faire. Help your Meal est là pour changer tout ça.

Deux versions seront misent à disposition.
* Une version gratuite 
  * il sera possible d'entrer en stock des produits en **quantités simultanées limitées** et de marquer leurs dates de peremptions afin de recevoir une alerte par mail, ou par notification sur l'appli mobile, quelques jours avant.

* Une version premium 
  * mêmes caractéristiques que la version gratuite
  * une gestion de stock minimum, envoyant une alerte quand un produit arrive à la limite de stockage et qu'il faut en acheter.    
  * un système de conception de recettes automatiques et personalisées avec les restes en stock.

## But de l'application

En plus d'aider les foyers, cette application a pour finalité d'aider la planète. Car en apprenant à utiliser les restes, et à consommer les produits avant qu'il ne finisse dans une poubelle, on dit **adieu au gaspillage alimentaire**.

## Technologies utilisées

* API : NodeJs (Express, Sequelize)
* BDD : MySql
* Front : VueJs
* Mobile : flutter ?



